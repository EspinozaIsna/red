#!/bin/bash
#

ip=192.168.88.10
inverso=88.168.192
puerta=192.168.88.1
dominio=equipoa.com

rm /etc/netplan/50-cloud-init.yaml
echo "

network:
  ethernets:
    eth0:
      addresses:
      - $ip/24
      gateway4: $puerta
      nameservers:
        addresses:
        - $ip
  version: 2 " >> /etc/netplan/50-cloud-init.yaml

netplan apply

rm /etc/bind/$dominio.db

echo '
;
; BIND data file for local loopback interface
;
$TTL  604800' "
@ IN  SOA $dominio. root.$dominio. (
            2   ; Serial
       604800   ; Refresh
        86400   ; Retry
      2419200   ; Expire
       604800 ) ; Negative Cache TTL
;
@ IN  NS  $dominio.
@ IN  A $ip
@ IN  MX   0  $dominio.
ns  IN  A $ip
$dominio.  IN  A $ip
www IN  A $ip
uah IN  CNAME $dominio.
blog  IN  A $ip
www.blog.$dominio. IN  A $ip
email IN  A $ip
1w  IN  MX  10  email.$dominio.
www.email.$dominio.  IN  A $ip" >> /etc/bind/$dominio.db

rm /etc/bind/named.conf.local

echo '
zone "'"$dominio"'"{
  type master;
  file "/etc/bind/'"$dominio"'.db";
  allow-update { none; };
};

zone "'"$inverso"'.in-addr.arpa"{
  type master;
  file "/etc/bind/inverso.db";
  allow-update { none; };
};

' >> /etc/bind/named.conf.local

rm /etc/resolv.conf

echo "
nameserver 127.0.0.1 " >> /etc/resolv.conf

rm /etc/ldap/ldap.conf
echo "
BASE    dc=equipoa,dc=com
URI     ldap://$ip:389

#SIZELIMIT    12
#TIMELIMIT    15
#DEREF        naver

#TLS certificates (needed for GnuTLS)
TLS_CACERT    /etc/ssl/certs/ca-certificates.crt

" >> /etc/ldap/ldap.conf

systemctl restart bind9
systemctl restart apache2


